/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package porto;
import java.util.*;

/**
 *
 * @author joshi
 */
public class Porto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        double l, b, h, g;        
        
        l=140000;
        b=90;
        h=3;
        g=10;
        
        String ausgabe = classify(l, b, h, g);
        System.out.println(ausgabe);
        
    }
    
    public static String classify(double l, double b, double h, double g) {
        
        double[][] table = {{},{},{}};
        
        String art = "ungültig";                                           
        
        
        //Maxibrief Plus
        if(l>=100 && b>=70 && l<=600 && b<=300 && h<=150 && g<=2000 && l+b+h <= 900 && l<=600 && b<=600 && h<=600){
            art = "Maxibrief Plus";
        }
        
        //Maxibrief
        if(l>=100 && b>=70 && l<=353 && b<=250 && h<=50 && g<=1000){
            art = "Maxibrief";
        }
        
        //Großbrief
        if(l>=100 && b>=70 && l<=353 && b<=250 && h<=20 && g<=500){
            art = "Großbrief";
        }
        
        //Kompaktbrief
        if(l>=100 && b>=70 && l<=235 && b<=125 && h<=10 && g<=50 && l >= b*1.4){
            art = "Kompaktbrief";
        }
        
        //Standardbrief
        if(l>=140 && b>=90 && l<=235 && b<=125 && h<=5 && g<=20 && l >= b*1.4){
            art = "Standardbrief";
        }                        
        
        return art;
    }
    
}
